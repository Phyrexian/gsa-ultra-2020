from collections import deque

def solution(a, b):
    winner = [[ 0, 2, 1, -1], [], [-1, 2, 0, 1], [1, 2, -1, 0]]
    qa, qb, rd = deque(a), deque(b), 0
    while len(qa) > 0 and len(qb) > 0:
        ca, cb = qa.pop(), qb.pop()
        rd += 1
        w = winner[ord(ca) - ord('P')][ord(cb) - ord('P')]
        if w == 0:
            qa.appendleft(ca)
            qb.appendleft(cb)
        elif w == 1:
            qa.appendleft(cb)
            qa.appendleft(ca)
        else:
            qb.appendleft(ca)
            qb.appendleft(cb)
    return rd
    