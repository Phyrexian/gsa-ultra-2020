def merge(a, b):
    l = min(a[0] - a[1], b[0] - b[1])
    r = max(a[0] + a[1], b[0] + b[1])
    mid = (l + r) / 2
    pwr = a[1] + abs(a[0] - mid)
    pwr = max(pwr, b[1] + abs(b[0] - mid))
    return (mid, pwr)

def solution(targets):
    targets.sort()
    bombs = []
    for t in targets:
        bombs.append(t)
        while len(bombs) > 1:
            big = merge(bombs[-1], bombs[-2])
            if big[1] > bombs[-1][1] + bombs[-2][1]:
                break
            bombs.pop()
            bombs.pop()
            bombs.append(big)
    return int(sum(bomb[1] for bomb in bombs))
    