spf = []

def sieve(n):
    global spf
    spf = [1] * n
    for i in range(2, n):
        spf[i] = i
    for i in range(2, n):
        if spf[i] == i:
            for j in range(2 * i, n, i):
                spf[j] = min(spf[j], i)

def solve(s, mx):
    global spf
    c = []
    result = 0
    for i in range(len(s)):
        if s[i] == '0':
            continue
        num = 0
        for j in range(i, len(s)):
            num *= 10
            num += ord(s[j]) - ord('0')
            if num >= mx:
                break
            if num > 1 and spf[num] == num:
                c.append((num, i, j))
    c.sort(reverse=True)
    for i in range(min(len(c), 2)):
        if i > 0 and c[i - 1][2] - c[i - 1][1] > c[i][2] - c[i][1]:
            break
        result = max(result, c[i][0] + solve(s[:c[i][1]] + s[c[i][2]+1:], c[i][0]))
    return result

def solution(s):
    sieve(10000)
    return solve(s, 10000)
    