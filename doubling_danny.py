def solution(a, m):
    pows, index, power = [2 % m], 1, 2
    while power < m:
        pows.append((pows[index - 1] * pows[index - 1]) % m)
        index += 1
        power <<= 1

    n = 7
    N = (1 << n)
    while len(pows) % n != 0:
        pows.append((pows[index - 1] * pows[index - 1]) % m)
        index += 1
    segs = len(pows) // n
    memo = []
    for k in range(segs):
        memo.append([])
        for i in range(N):
            pro = 1
            for j in range(n):
                if i & (1 << j):
                    pro = (pro * pows[k * n + j]) % m
            memo[-1].append(pro)

    a = [ x % (m - 1) for x in a ]
    s = 0
    for x in a:
        bits = [0] * len(pows)
        index, power = len(pows) - 1, 1 << (len(pows) - 1)
        while x > 0:
            if power <= x:
                bits[index] = 1
                x -= power
            index -= 1
            power >>= 1
        ints = [0] * segs
        for k in range(segs):
            for i in range(n):
                if bits[k * n + i] == 1:
                    ints[k] |= 1 << i
        pro = 1
        for k in range(segs):
            pro = (pro * memo[k][ints[k]]) % m
        s = (s + pro) % m
    return s
    