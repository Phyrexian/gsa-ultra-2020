import math

def solution(a, qs):
    N = len(a)
    K = math.floor(math.log(N, 2))
    st = [[ -1 for _ in range(K + 1)] for _ in range(N)]
    for i in range(0, N):
        st[i][0] = a[i]
    for j in range(1, K + 1):
        for i in range(0, N + 1 - (1 << j)):
            st[i][j] = max(st[i][j-1], st[i + (1 << (j - 1))][j - 1])
    log = [-1] * (N + 1)
    log[1] = 0
    for i in range(2, N + 1):
        log[i] = log[i >> 1] + 1
    s = 0
    for i in range(len(qs)):
        L, R = qs[i]
        j = log[R - L]
        s += max(st[L][j], st[R - (1 << j)][j])
    return s
    