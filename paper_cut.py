def ok(a, b, c, d):
    if (c * d) % (a * b) != 0:
        return False
    if c % a == 0 and d % b == 0:
        return True
    if d % a == 0 and d % b == 0:
        for i in range(0, c):
            if i * a > c:
                break
            if (c - i * a) % b == 0:
                return True
    return False

'''
def ok(a, b, c, d):
    if (c * d) % (a * b) == 0:
        return True
    return False
'''

def solution(qs):
    s, m = 0, 10 ** 9 + 7
    for i in range(len(qs)):
        (a, b, c, d) = qs[i]
        if ok(a, b, c, d) or ok(a, b, d, c):
            s = (s + pow(2, i, m)) % m
    return s
    