import random

class TreapNode:
    def __init__(self, value, priority, left, right):
        self.value = value
        self.priority = priority
        self.left = left
        self.right = right
        self.update_max()

    def __repr__(self):
        return "<TreapNode value:%s max_length:%s>" % (self.value, self.max_length)
    
    def update_max(self):
        self.max_length = self.value[1] - self.value[0]
        if self.left and self.left.max_length > self.max_length:
            self.max_length = self.left.max_length
        if self.right and self.right.max_length > self.max_length:
            self.max_length = self.right.max_length
    
def merge(t0, t1):
    if t0 and t1:
        if t0.priority > t1.priority:
            t0.right = merge(t0.right, t1)
            t0.update_max()
            return t0
        else:
            t1.left = merge(t0, t1.left)
            t1.update_max()
            return t1
    elif t0:
        return t0
    elif t1:
        return t1
    return None

def split(t, val):
    if t:
        if val > t.value:
            t0, t1 = split(t.right, val)
            t.right = t0
            t.update_max()
            return t, t1
        else:
            t0, t1, = split(t.left, val)
            t.left = t1
            t.update_max()
            return t0, t
    return None, None

def contains(t, val):
    if t:
        if val == t.value:
            return True
        elif val < t.value:
            return contains(t.left, val)
        else:
            return contains(t.right, val)
    return False

def insert(t, val, pri):
    if t:
        if pri > t.priority:
            t0, t1 = split(t, val)
            return TreapNode(val, pri, t0, t1)
        elif val < t.value:
            t.left = insert(t.left, val, pri)
        else:
            t.right = insert(t.right, val, pri)
        t.update_max()
        return t
    return TreapNode(val, pri, None, None)

def erase(t, val):
    if val == t.value:
        return merge(t.left, t.right)
    elif val < t.value:
        t.left = erase(t.left, val)
    else:
        t.right = erase(t.right, val)
    t.update_max()
    return t

# find key of left-most interval with at least a certain length
def find(t, length):
    if t and t.max_length >= length:
        key = find(t.left, length)
        if key:
            return key
        if t.value[1] - t.value[0] >= length:
            return t.value
        return find(t.right, length)
    return None

def floor(t, val):
    if t:
        if t.value == val:
            return t.value
        if t.value > val:
            return floor(t.left, val)
        tmp = floor(t.right, val)
        if tmp and tmp <= val:
            return tmp
        return t.value
    return None

def ceil(t, val):
    if t:
        if t.value == val:
            return t.value
        if t.value < val:
            return ceil(t.right, val)
        tmp = ceil(t.left, val)
        if tmp and tmp >= val:
            return tmp
        return t.value
    return None

def solution(n, rs):
    root = None
    root = insert(root, (0, n), random.random())
    chkd = [ ]
    s = 0
    for i in range(len(rs)):
        if rs[i][0] == 'I':
            seg = find(root, rs[i][1])
            chkd.append((seg[0], seg[0] + rs[i][1]))
            root = erase(root, seg)
            if chkd[-1][1] < seg[1]:
                root = insert(root, (chkd[-1][1], seg[1]), random.random())
        else:
            seg = chkd[rs[i][1]]
            fl = floor(root, seg)
            ce = ceil(root, seg)
            if fl and fl[1] == seg[0]:
                root = erase(root, fl)
                seg = (fl[0], seg[1])
            if ce and ce[0] == seg[1]:
                root = erase(root, ce)
                seg = (seg[0], ce[1])
            root = insert(root, seg, random.random())
            chkd[rs[i][1]] = None
    return sum([ i * chkd[i][0] if chkd[i] else 0 for i in range(len(chkd)) ])
    