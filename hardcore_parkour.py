import math, sys
sys.setrecursionlimit(10**6) 

def sign(x):
    if x > 0:
        return 1
    if x < 0:
        return -1
    return 0

def clamp(x, lo, hi):
    return max(min(x, hi), lo)

def solve(memo, ps, i, h, spd):
    if (i, h, spd) in memo:
        return memo[(i, h, spd)]
    if i == len(ps):
        return 0
    res = 10 ** 18
    for jmp in range(-5, 6):
        if h + jmp in ps[i]:
            newspd = clamp(spd + sign(jmp), 1, 10)
            res = min(res, newspd + solve(memo, ps, i + 1, h + jmp, newspd))
    memo[(i, h, spd)] = res
    return memo[(i, h, spd)]

def solution(ps):
    a = []
    for i in range(len(ps)):
        s = set()
        for j in range(len(ps[i])):
            s.add(ps[i][j])
        a.append(s)
    memo = {}
    res = 10 ** 18
    for h in a[0]:
        res = min(res, 5 + solve(memo, a, 1, h, 5))
    return res
    