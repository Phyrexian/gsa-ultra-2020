import sys
sys.setrecursionlimit(10**6) 

def solve(memo, a, b, pos, i, j):
    if (pos, i, j) in memo:
        return memo[(pos, i, j)]
    if i == len(a) and j == len(b):
        memo[(pos, i, j)] = 0
    elif i == len(a):
        memo[(pos, i, j)] = abs(pos - b[j]) + solve(memo, a, b, b[j], i, j + 1)
    elif j == len(b):
        memo[(pos, i, j)] = abs(pos - a[i]) + solve(memo, a, b, a[i], i + 1, j)
    else:
        memo[(pos, i, j)] = min(abs(pos - b[j]) + solve(memo, a, b, b[j], i, j + 1), abs(pos - a[i]) + solve(memo, a, b, a[i], i + 1, j))
    return memo[(pos, i, j)]

def solution(a, b):
    memo = {}
    return solve(memo, a, b, 0, 0, 0)

print(solution([5, 3, 10, 6], [9, 7, 12]))
