class Var:
    def __init__(self, x):
        self.x, self.subset = x, x

    def assign(self, bit, value):
        if self.subset & bit:
            return value
        return self

    def __repr__(self):
        return str(self.x)

class Not:
    def __init__(self, x):
        self.x, self.subset = x, x.subset

    def assign(self, bit, value):
        if self.subset & bit:
            x = self.x.assign(bit, value)
            if type(x) == bool:
                return not x
            return Not(x)
        return self
    
    def __repr__(self):
        return "NOT(%s)" % (self.x)

class And:
    def __init__(self, x, y):
        self.x, self.y, self.subset = x, y, x.subset | y.subset

    def assign(self, bit, value):
        if self.subset & bit:
            x = self.x.assign(bit, value)
            if type(x) == bool:
                if x:
                    return self.y.assign(bit, value)
                else:
                    return False
            y = self.y.assign(bit, value)
            if type(y) == bool:
                if y:
                    return x
                else:
                    return False
            if type(x) == bool and type(y) == bool:
                return True
            return And(x, y)
        return self
    
    def __repr__(self):
        return "(%s AND %s)" % (self.x, self.y)
    
class Or:
    def __init__(self, x, y):
        self.x, self.y, self.subset = x, y, x.subset | y.subset

    def assign(self, bit, value):
        if self.subset & bit:
            x = self.x.assign(bit, value)
            if type(x) == bool:
                if x:
                    return True
                else:
                    return self.y.assign(bit, value)
            y = self.y.assign(bit, value)
            if type(y) == bool:
                if y:
                    return True
                else:
                    return x
            if type(x) == bool and type(y) == bool:
                return False
            return Or(x, y)
        return self
    
    def __repr__(self):
        return "(%s OR %s)" % (self.x, self.y)

def count_true(prop, i):
    if type(prop) == bool:
        return 1 << i if prop else 0
    return count_true(prop.assign(1 << (i - 1), False), i - 1) + count_true(prop.assign(1 << (i - 1), True), i - 1)

def solution(n, proposition):
    proposition = proposition.replace('(', '( ')
    proposition = proposition.replace(')', ' )')
    proposition = proposition.split()
    newprop = ['('] * 3
    for s in proposition:
        if s == '(':
            newprop += ['('] * 3
        elif s == ')':
            newprop += [')'] * 3
        elif s == 'AND':
            newprop += [')', 'AND', '(']
        elif s == 'OR':
            newprop += [')', ')', 'OR', '(', '(']
        elif s == 'NOT':
            newprop += ['NOT']
        else:
            newprop += [s]
    newprop += [')'] * 3
    prop_stack, op_stack = [], []
    for s in newprop:
        if s == ')':
            while True:
                popped = op_stack.pop()
                if popped == '(':
                    break
                elif popped == 'NOT':
                    x = prop_stack.pop()
                    prop_stack.append(Not(x))
                elif popped == 'AND':
                    y = prop_stack.pop()
                    x = prop_stack.pop()
                    prop_stack.append(And(x, y))
                elif popped == 'OR':
                    y = prop_stack.pop()
                    x = prop_stack.pop()
                    prop_stack.append(Or(x, y))
        elif len(s) == 1 and s.isalpha():
            prop_stack.append(Var(1 << (ord(s) - ord('A'))))
        else:
            op_stack.append(s)
    prop = prop_stack[0]
    return count_true(prop, n)

#print(solution(3, "A AND NOT NOT B OR C AND NOT (A OR B)"))
