#from fractions import Fraction
from itertools import combinations
from math import factorial, gcd

def solve(n, skills):
    ones = [(1 << i) for i in range(n)]
    dp = [None] * (1 << n)
    for i in range(n):
        dp[ones[i]] = [(1, 1) if j == i else (0, 1) for j in range(n)]
    
    k = 2
    while k <= n:
        n_small_combs = factorial(k) // factorial(k // 2) ** 2
        n_small_combs //= 2
        big_combs = list(combinations(ones, k))
        for big_comb in big_combs:
            c = sum(big_comb)
            dp[c] = [(0, 1) for _ in range(n)]
            small_combs = list(combinations(big_comb, k // 2))
            for itr, small_comb in enumerate(small_combs):
                a = sum(small_comb)
                b = c - a
                for i in range(n):
                    if not dp[a][i][0]:
                        continue
                    for j in range(n):
                        if not dp[b][j][0]:
                            continue
                        num = dp[a][i][0] * dp[b][j][0]
                        den = dp[a][i][1] * dp[b][j][1] * (skills[i] + skills[j])
                        dp[c][i] = (dp[c][i][0] * den + num * skills[i] * dp[c][i][1], dp[c][i][1] * den)
                        tmp = gcd(dp[c][i][0], dp[c][i][1])
                        dp[c][i] = (dp[c][i][0] // tmp, dp[c][i][1] // tmp)
                        dp[c][j] = (dp[c][j][0] * den + num * skills[j] * dp[c][j][1], dp[c][j][1] * den)
                        tmp = gcd(dp[c][j][0], dp[c][j][1])
                        dp[c][j] = (dp[c][j][0] // tmp, dp[c][j][1] // tmp)
                if itr == n_small_combs - 1:
                    break
            for i in range(n):
                dp[c][i] = (dp[c][i][0], dp[c][i][1] * n_small_combs)
                tmp = gcd(dp[c][i][0], dp[c][i][1])
                dp[c][i] = (dp[c][i][0] // tmp, dp[c][i][1] // tmp)
        k *= 2
    return dp[-1][0]


def solution(skills):
    keys = list(skills.keys())
    if keys[0] != 'Andy':
        index = keys.index('Andy')
        keys[0], keys[index] = keys[index], keys[0]
    skills = [skills[key] for key in keys]
    n = len(skills)

    f = solve(n, skills)
    return str(f[0]) + str(f[1])


#print(solution({'Andy': 7, 'Novak': 5, 'Roger': 3, 'Rafael': 2}))
#print(solution({'Andy': 7, 'Novak': 5, 'Roger': 3, 'Rafael': 2, '_Andy': 7, '_Novak': 5, '_Roger': 3, '_Rafael': 2}))
'''
import time
t0 = time.time()
print(solution({'Andy': 20, 'Novak': 19, 'Roger': 18, 'Rafael': 17, '_Andy': 16, '_Novak': 15, '_Roger': 14, '_Rafael': 13, '__Andy': 12, '__Novak': 11, '__Roger': 10, '__Rafael': 9, '___Andy': 8, '___Novak': 7, '___Roger': 6, '___Rafael': 5}))
print(time.time() - t0)'''
